const path = require('path');

const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin'); //installed via npm
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const buildPath = path.resolve(__dirname, './../dist');

const basePath = __dirname;
const targetPath = './../';
const targetFolder = 'dist';

module.exports = {
    // devtool: 'source-map',
    entry: './src/index.js',
    output: {
        filename: '[name].[hash:20].js',
        path: buildPath
    },
    node: {
        fs: 'empty'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',

                options: {
                    sourceMap: false,
                    presets: ['env']
                }
            },
            {
                test: /\.(scss|css|sass)$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        // translates CSS into CommonJS
                        loader: 'css-loader',
                        options: {
                            sourceMap: false
                        }
                    },
                    {
                        // Runs compiled CSS through postcss for vendor prefixing
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: false
                        }
                    },
                    {
                        // compiles Sass to CSS
                        loader: 'sass-loader',
                        options: {
                            outputStyle: 'expanded',
                            sourceMap: false,
                            sourceMapContents: false
                        }
                    }
                ]
            },
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: '[name].[hash:20].[ext]',
                            limit: 8192
                        }
                    }
                ]
            },
            {
              test: /\.(mov|mp4|ico)$/,
              use: [
                'file-loader'
              ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './index.html',
            // Inject the js bundle at the end of the body of the given template
            inject: 'body',
        }),
        // new CleanWebpackPlugin(buildPath),
        new CleanWebpackPlugin([targetFolder], {
          root: basePath + '/' + targetPath
        }),
        // new FaviconsWebpackPlugin({
        //     // Your source logo
        //     logo: './src/assets/logo-1463724445.jpg',
        //     // The prefix for all image files (might be a folder or a name)
        //     prefix: 'icons-[hash]/',
        //     // Generate a cache file with control hashes and
        //     // don't rebuild the favicons until those hashes change
        //     persistentCache: true,
        //     // Inject the html into the html-webpack-plugin
        //     inject: true,
        //     // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
        //     background: '#fff',
        //     // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
        //     title: 'EnigmaFBLanding}}',
        //
        //     // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
        //     icons: {
        //         android: true,
        //         appleIcon: true,
        //         appleStartup: true,
        //         coast: false,
        //         favicons: true,
        //         firefox: true,
        //         opengraph: false,
        //         twitter: false,
        //         yandex: true,
        //         windows: false
        //     }
        // }),
        new MiniCssExtractPlugin({
            filename: 'styles.[contenthash].css'
        }),
        new OptimizeCssAssetsPlugin({
            cssProcessor: require('cssnano'),
            cssProcessorOptions: {
                // map: {
                //     inline: false,
                // },
                discardComments: {
                    removeAll: true
                }
                // sourceMap: false
            },
            canPrint: false
        }),
        new CopyPlugin([
          { from: './src/php', to: './../dist/php/' },
        ]),
    ]
};
