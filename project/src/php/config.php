<?php
############################################
// Input configuration here!
$req=['phone']; // required fields for all forms
$trans=array(	// translations for form fields - if you ever need to localize errors
	"name" => "имя",
	"phone" => "телефон",
	"email" => "email",
);

$messageSuccess='Ваше сообщение было отправлено успешно!';
$messageSuccessText='Наш менеджер свяжется с вами в ближайшее время.';
$messageError='Сообщение не было отправлено! Ошибка: ';
$err_string='Мы не можем принять вашу заявку. Вам необходимо указать:'; // translations for form fields - if you ever need to localize errors

$redirect=null;

$site_name='Лендинг для платья - enigma';
$addr='enigma-elit.com';

$sender='robot@enigma-elit.com';
$emails=['enigmadresses@gmail.com']; // set up as many emails as you wish; one per array element
$BCCemails=['zakaz@progon.od.ua', 'yarosh.alexandr@gmail.com']; // set up as many emails as you wish; one per array element

############################################
