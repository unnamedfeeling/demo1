
require('normalize.css/normalize.css');
// require('!style-loader!css-loader!video.js/dist/video-js.css')
require('./styles/index.scss');

import Glide from '@glidejs/glide';
import videojs from 'video.js';


var WebFont = require('webfontloader');
WebFont.load({
  google: {
    families: ['Montserrat:100,300,400,500,600,700,900']
  }
});
var Timer = require('easytimer.js').Timer;

/**
 * Takes a form node and sends it over AJAX.
 * @param {HTMLFormElement} form - Form node to send
 * @param {function} callback - Function to handle onload.
 *                              this variable will be bound correctly.
 */

var sendData = function (data, opts) {
    return new Promise(function(resolve, reject){
      var XHR = new XMLHttpRequest();
      // var urlEncodedData = data;
      var FD  = new FormData();
      var res='';

      var urlencodeFormData=function(fd){
        var s = '', pair;
        function encode(s){ return encodeURIComponent(s).replace(/%20/g,'+'); }
        for(pair of fd.entries()){
          if(typeof pair[1]=='string'){
            s += (s?'&':'') + encode(pair[0])+'='+encode(pair[1]);
          }
        }
        return s;
      }

      // Define what happens on successful data submission
      XHR.addEventListener('loadend', function(event) {
        if ([200, 201].indexOf(XHR.status) == -1) {
          res=XHR.status + ': ' + XHR.statusText;
          reject(res);
        } else {
          res=XHR.responseText;
          resolve(res);
        }
      });

      // Define what happens in case of error
      XHR.addEventListener('error', function(event) {
        reject(XHR);
      });

      // Push our data into our FormData object


      // Set up our request
      opts.type=(typeof opts.type !== 'undefined')?opts.type:'POST';
      opts.url=(typeof opts.url !== 'undefined')?opts.url:null;

      var optName='';
      switch (opts.type) {
        case 'POST-JSON':
          FD=JSON.stringify(data);
          break;
        case 'POST':
          for(optName in data){
            FD.append(optName, data[optName]);
          }
          break;
        default:
          opts.url+='?';
          for(optName in data){
            opts.url+=optName+'='+data.apiParams[optName]+'&';
          }
      }

      var xhrType=(opts.type.indexOf('-JSON')>=0)?opts.type.replace('-JSON', ''):opts.type;

      XHR.open(xhrType, opts.url);

      // Add the required HTTP header for form data POST requests
      XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

      // Finally, send our data.

      // console.log(data);
      if (opts.type=='GET') {
        XHR.send();
      } else if(opts.type=='POST-JSON') {
        // XHR.setRequestHeader('Content-Type', 'application/json');
        XHR.send(FD);
      } else {
        XHR.send(urlencodeFormData(FD));
      }
    });
  },
  // makeRequest({data: Object data, opts: Object opts, fullfiled: function fullfiled, rejected: function rejected});
  makeRequest = function(params){
    if (typeof params=='undefined') {
      console.log({msg: 'Empty params for sendData!'});
    }

    return new Promise(function(resolve, reject){
      sendData(params.data, params.opts).then(
        function(result){
          try {
            result = (typeof result==='string')?JSON.parse(result):result;
          } catch(e) {
            console.log({msg: e});
            console.log({msg: result});
            reject(e);
          }
          // app.log({msg: 'fullfiled:'});
          // app.log({msg: result});
          if (typeof params.fullfiled=='function') {
            params.fullfiled(result);
          }
          resolve();
        },
        function(result){
          try {
            result = (typeof result==='string')?JSON.parse(result):result;
          } catch(e) {
            console.log({msg: e});
            console.log({msg: result});
            reject(e);
          }
          console.log({msg: 'rejected:'});
          console.log({msg: result});
          if (typeof params.rejected=='function') {
            params.rejected(result);
          }
          reject(result);
        }
      );
    })
  }

document.addEventListener("DOMContentLoaded", () => {
  // init all sliders
  var sliders = document.querySelectorAll('.js-slider')
  sliders.forEach(function(sld){
    var slides=sld.querySelectorAll('img'),
      count=slides.length,
      slidesToLoad=[slides[0], slides[1], slides[count-1]]

    slidesToLoad.forEach(function(img){
      img.src=img.dataset.lazy
      img.removeAttribute('data-lazy')
    })

    var glideSlider = new Glide(sld,{
      type: 'carousel',
      perView: 1,
      focusAt: 'center',
      autoplay: 5000
    })

    sld.querySelectorAll('.js-sliderArrow').forEach(function(arrow){
      arrow.addEventListener('click', function(event){
        glideSlider.go(event.target.dataset.glideDir)
      })
    })

    glideSlider.on('run.before', function(e) {
      var next=glideSlider.index+((e.direction=='<')?-2:2)
      if (slides[next]!==undefined&&slides[next].dataset.lazy!==undefined) {
        slides[next].src=slides[next].dataset.lazy
        slides[next].removeAttribute('data-lazy')
      }
    })

    window.addEventListener('load', function(){
      glideSlider.mount()
    })

  })



  // init all timers
  var timer = new Timer(),
    timerElements= document.querySelectorAll('.js-timer')
    // now=Date.now(),
    // endTime=addHours(now, 24);

  timer.start({
    precision: 'seconds',
    countdown: true,
    startValues: {days: 1}
  })
  timerElements.forEach(function(t){
    var curT=timer.getTimeValues(),
      digits=t.querySelectorAll('.digit'),
      h=(curT.hours.toString().length<2)?'0'+curT.hours.toString():curT.hours.toString(),
      m=(curT.minutes.toString().length<2)?'0'+curT.minutes.toString():curT.minutes.toString(),
      s=(curT.seconds.toString().length<2)?'0'+curT.seconds.toString():curT.seconds.toString(),
      timeArr=(h+m+s).split(''),
      i=0;

    for (i = 0; i < digits.length; i++) {
      digits[i].innerHTML = timeArr[i]
    }


    timer.addEventListener('secondsUpdated', function (e) {
      curT=timer.getTimeValues(),
      h=(curT.hours.toString().length<2)?'0'+curT.hours.toString():curT.hours.toString(),
      m=(curT.minutes.toString().length<2)?'0'+curT.minutes.toString():curT.minutes.toString(),
      s=(curT.seconds.toString().length<2)?'0'+curT.seconds.toString():curT.seconds.toString(),
      timeArr=(h+m+s).split('');

      for (i = 0; i < digits.length; i++) {
        digits[i].innerHTML = timeArr[i]
      }
    });
  })


  // handle videos
  var videos = document.querySelectorAll('.js-video')
  if (videos.length) {
    var settings=['id', 'src', 'preload', 'controls', 'poster']
    window.addEventListener('load', function(){
      videos.forEach(function(vid){
        var injectElem=document.createElement('video')
        injectElem.classList.add('video-js')

        settings.forEach(function(setting){
          switch (setting) {
            case 'id':
            case 'src':
            case 'poster':
            case 'preload':
              if (typeof vid.dataset[setting]!=='undefined') {
                injectElem[setting]=vid.dataset[setting]
              }
            case 'controls':
              if (typeof vid.dataset[setting]!=='undefined') {
                injectElem.controls=true
              }
          }
        })

        const options = {
          sources: [{
            src: injectElem.src,
            type: 'video/mp4'
          }]
        }
        vid.appendChild(injectElem)

        videojs(injectElem.id, options);
      })
    })
  }

  // anchors
  var anchors=document.querySelectorAll('.js-anchor')
  if (anchors.length) {
    anchors.forEach(function(a){
      if (a.dataset.target!==undefined) {
        a.addEventListener('click', function(){
          document.querySelector(a.dataset.target).scrollIntoView({block: "center", behavior: "smooth"})
        })
      }

    })
  }

  var seoData={
      getSearchParameters: function() {
      	var prmstr = window.location.search.substr(1);
      	return prmstr != null && prmstr != "" ? this.transformToAssocArray(prmstr) : {};
      },
      transformToAssocArray:function ( prmstr ) {
      	var params = {};
      	var prmarr = prmstr.split("&");
      	for ( var i = 0; i < prmarr.length; i++) {
      		var tmparr = prmarr[i].split("=");
      		params[tmparr[0]] = tmparr[1];
      	}
      	return params;
      },
      init: function(){
        var result=this.getSearchParameters(),
          findGet=window.location.href.indexOf('?'),
          payload = {},
          opts = {url: 'https://api.ipgeolocation.io/ipgeo?apiKey=d2cb9bf5fce546dd975c84a3479079d2', type: 'GET'}

        var request = makeRequest({
          data: payload,
          opts: opts,
          fullfiled: function(geodata) {
           result.utm_ip=geodata.ip;
           result.utm_city=geodata.city;
           result.utm_country=geodata.country_name;

           result.utm_useragent=navigator.userAgent
           if (findGet!=-1) {
             result.utm_page=window.location.href.substring(0, findGet)
           } else {
             result.utm_page=window.location.href
           }
           result.utm_prevpage=document.referrer
          },
          rejected: function(geodata){console.log(geodata); return geodata}
        })

        return result
      }
    },
    submitParams=seoData.init()

  document.querySelectorAll('form').forEach(function(form){
    form.addEventListener('submit', function(event){
      event.preventDefault();

      var payload = {},
        opts = {url: form.action, type: 'POST'},
        fields = form.querySelectorAll('input,textarea');

      fields.forEach(function(field){
        if (field.classList.contains('error')) {
          field.classList.remove('error')
        }
        payload[field.name]=field.value;
      });

      Object.keys(submitParams).forEach(function(key,index) {
        payload[key]=submitParams[key];
			});

      var successBtn = form.querySelector('button')
      successBtn.disabled=true

      makeRequest({
        data: payload,
        opts: opts,
        fullfiled: function(data){
          if(data.error!==1){
    				successBtn.innerText  = 'Успешно отправлено';
            successBtn.classList.add('success')

            if (typeof fbq!=='undefined') {
              fbq('track', 'Lead')
            }
    			} else {
    				data.errfields.forEach(function(el){
              form.querySelector('[name="'+el+'"]').classList.add('error')
            })
            successBtn.disabled=false
    			}
        },
        rejected: function(data){console.log(data);}
      });
    })
  })

});
