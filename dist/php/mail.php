<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
header('Content-Type: application/json');
$json=[];
$json['error']=1;
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	header("HTTP/1.0 400 Bad Request");
	$json['status'] = 'error';
	$json['msg']='Not allowed!';
	die(json_encode( $json ));
} else {
	require '../../vendor/autoload.php';
	include 'config.php';

	$payload=$_POST;

	$json['post']=$payload;

	$t='';
	$errfields=[];
	foreach ($req as $val) {
		if (empty($_POST[$val])) {
			$errfields[]=$val;
			$t.='<li>'.$val.'</li>';
		}
	}
	if(!empty($t)){
		$json['error'] = 1;
		$json['status'] = 'error';
		$t=strtr($t, $trans);
		$json['msg'] = $err_string.' <ul>' . $t . '</ul>';
		$json['errfields']=$errfields;
		header('Content-type: application/json');
		die(json_encode($json));
	}

	// form mail body
	$body="<h2 style=\"margin-bottom:20px\">Новое сообщение с сайта: ".$addr."</h2><table>";
	$message='';
	foreach ( $_POST as $key => $value ) {
		if ( $value != "" ) {
			if($key == 'addata'){$key='Данные об отправителе';}
			if($key == 'countrydata'){$key='Данные о стране отправителя';}
			if($key == 'name'){$key='Имя';}
			if($key == 'phone'){$key='Телефон';}
			if($key == 'phonedata'){$key='Полный телефон';}
			if($key == 'email'){$key='Почта';}
			if($key == 'comment'){$key='Комментарий';}
			if($key == 'text'){$key='Сообщение';}
			if($key == 'utm_prevpage'){$key='Перешел с';}
			if($key == 'utm_page'){$key='Отправлено с';}
			if($key == 'utm_city'){$key='Город';}
			if($key == 'utm_country'){$key='Страна';}
			if($key == 'utm_useragent'){$key='Браузер';}
			if($key == 'utm_ip'){$key='IP';}
			if($key == 'formservices'){continue;}
			if($key == 'reqFields'){continue;}
			if($key == 'detects'){continue;}
			if($key == 'color'){continue;}
			// if($key == 'fname'){$key='Форма';}
			$message .= "
			" . ( ($c = !$c) ? '<tr>':'<tr style="background-color: #f8f8f8;">' ) . "
			<td style='padding: 10px; border: #e9e9e9 1px solid;'><b>$key</b></td>
			<td style='padding: 10px; border: #e9e9e9 1px solid;'>$value</td>
		</tr>
		";
		}
	}
	$body.=$message;
	$body.='</table>';

	// $fname=str_replace('/', DIRECTORY_SEPARATOR, '/messages/message-'.date('H:i:s__d-m-Y').'.txt');
	// $fname=addslashes(__DIR__.DIRECTORY_SEPARATOR.'message-'.date('H-i-s__d-m-Y').'.txt');
	$fname=__DIR__.'/../../messages/message-'.date('H-i-s__d-m-Y').'.txt';
	$messageFile=fopen($fname, 'w+');
	fwrite($messageFile, $body);
	fclose($messageFile);

	if (!empty($emails)) {
		$mail = new PHPMailer;
		$mail->CharSet = "UTF-8";
		$mail->setFrom($sender, 'Сайт '.$site_name);
		foreach ($emails as $val) {
			$mail->addAddress($val);
		}
		if (!empty($BCCemails)) {
			foreach ($BCCemails as $val) {
				$mail->addBCC($val);
			}
		}
		$mail->Subject = 'Новая форма с сайта '.$site_name;
		$mail->isHTML(true);
		$mail->Body = $body;

		//send webhook to zapier
		$json['datetime']=date('d-m-Y H:i:s');

		switch (true) {
			case (!empty($payload['phone'])):
				$valuableString=(!empty($_POST['phonedata']))?$_POST['phonedata']:$_POST['phone'];
				$valuableString=(strpos( $valuableString, '+380' )==0)?str_replace('+38', '', $valuableString):$valuableString;
				if (strpos( $valuableString, '0' )!==0) {
					$valuableString='0'.$valuableString;
				}
				break;

			case (!empty($payload['email'])):
				$valuableString=$payload['email'];
				break;
		}

		if(!$mail->send()) {
			$json['error'] = 1;
			$json['msg'] = $messageError . $mail->ErrorInfo;
			header('Content-type: application/json');
			exit(json_encode($json));
		} else {
			$json['error'] = 0;
			$json['redirect'] = $redirect;
			$json['msg'] = $messageSuccess;
			$json['submsg'] = $messageSuccessText;
			header('Content-type: application/json');
			exit(json_encode($json));
		}
	} else {
		$json['error'] = 0;
		$json['redirect'] = $redirect;
		$json['msg'] = $messageSuccess;
		exit(json_encode($json));
	}
}
